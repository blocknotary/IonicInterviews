angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $ionicPopup, $timeout) {
  $scope.showAlert = function() {
    var alertPopup = $ionicPopup.alert({
      title: 'Ionic Popup',
       template: 'This is alert popup',
    });
    alertPopup.then(function(res) {
      console.log('Thanks');
    });
  };

  $scope.runSDKDemo = function() {
    window.interview.setDelegate(delegateMethod, onFail);
    window.interview.showDemoInterview(onSuccess, onFail);

    function delegateMethod(delegate_message) {
      if (delegate_message == 'willAppear'){
            console.log('DELEGATE: Interview will appear');
      }

      if (delegate_message == 'didAppear'){
            console.log('DELEGATE: Interview did appear');
      }

      if (delegate_message == 'willDisappear'){
            console.log('DELEGATE: Interview will disappear');
            
      }

      if (delegate_message == 'didDisappear'){
            console.log('DELEGATE: Interview did disappear');
      }
    }

    function onSuccess(taskDictionary) {
      alert('Finished');
    }

    function onFail(error) {
      alert('Failed because: ' + message);
    }

  };


  $scope.runSDK = function() {
	
	var url = "https://videointerview.blocknotary.com",

        user = "@gmail.com",

        pass = "**",

	    userKey = "40512c57a74f5b1031aff1aaaf3665f93b608a6f042a32c8c792a8773451e450",

        taskid = "h8teS0XxiK",

        authString = "Basic YWxhZGRpbjpvcGVuc2VzYW1l";



    window.interview.setEngineWithKey(onSuccessSetEngine, onFailSetEngine, url, userKey, taskid);
//    window.interview.setEngine(onSuccessSetEngine, onFailSetEngine, "https://videointerview.blocknotary.com", "@gmail.com", "**", "**");
    

    function onSuccessSetEngine(taskDictionary) {

      console.log('SetEngine OK');

      window.interview.setAuthString(onSuccessSetAuthString, onFailSetAuthString, authString);

      window.interview.setS3Storage(onSuccessSetS3Storage, onFailSetS3Storage, "https://s3.amazonaws.com", "interviewsdk-bucket", "AKIAIXT3R3E53AXS3LZA", "VvkG5h7jfQ2/0IjTGBElmItAqr6nY9FjVyWMKgbT");

      window.interview.showInterview(onSuccess, onFail, taskid, "high");

    }

    function onFailSetEngine(error) {

      alert('Cannot set engine because: ' + message);

    }

    function onSuccessSetAuthString() {

      alert('Auth string set successfully');

    }


    function onFailSetAuthString(error) {

      alert('Cannot set auth string because: ' + message);

    }

    function onSuccessSetS3Storage() {

      alert('S3 storage set successfully');

    }


    function onFailSetS3Storage(error) {

      alert('Cannot set S3 storage because: ' + message);

    }

    function onSuccess(taskDictionary) {

      alert('Finished');

    }

    function onFail(error) {

      alert('Failed because: ' + message);

    }

  };
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

    $scope.chats = Chats.all();
    $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
