// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    window.interview.setDelegate(delegateMethod, onFail);
    window.interview.setS3Storage(onSuccessSetS3Storage, onFail, "https://s3.amazonaws.com", "interviewsdk-bucket", "AKIAIXT3R3E53AXS3LZA", "VvkG5h7jfQ2/0IjTGBElmItAqr6nY9FjVyWMKgbT");
    window.interview.showDemoInterview(onSuccess, onFail);

    function delegateMethod(delegate_message) {
      if (delegate_message == 'willAppear'){
            console.log('DELEGATE: Interview will appear');
      }

      if (delegate_message == 'didAppear'){
            console.log('DELEGATE: Interview did appear');
      }

      if (delegate_message == 'willDisappear'){
            console.log('DELEGATE: Interview will disappear');
            
      }

      if (delegate_message == 'didDisappear'){
            console.log('DELEGATE: Interview did disappear');
      }
    }

    function onSuccess(taskDictionary) {
      //alert('Finished');
    }

    function onSuccessSetS3Storage() {

      console.log('S3 storage set successfully');

    }

    function onFail(error) {
      alert('Failed because: ' + message);
    }

  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js


//  $stateProvider
//
// .state('tab', {
//    url: '/tab',
//    abstract: true,
//     templateUrl: 'templates/tabs.html'
//   })
//
// .state('tab.dash', {
//    url: '/dash',
//    views: {
//      'tab-dash': {
//        templateUrl: 'templates/tab-dash.html',
//        controller: 'DashCtrl'
//      }
//    }
//  })
//
// .state('tab.chats', {
//   url: '/chats',
//    views: {
//     'tab-chats': {
//        templateUrl: 'templates/tab-chats.html',
//         controller: 'ChatsCtrl'
//        }
//     }
//  })
//
// .state('tab.chat-detail', {
//    url: '/chats/:chatId',
//     views: {
//      'tab-chats': {
//        templateUrl: 'templates/chat-detail.html',
//        controller: 'ChatDetailCtrl'
//       }
//    }
//  })
//
// .state('tab.account', {
//   url: '/account',
//     views: {
//       'tab-account': {
//         templateUrl: 'templates/tab-account.html',
//         controller: 'AccountCtrl'
//      }
//    }
//  });
//
//  $urlRouterProvider.otherwise('/tab/dash');
});
